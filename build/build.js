import fs from "fs";
import {join, dirname} from "path";
import {fileURLToPath} from "url";
import {spawnSync} from "child_process";

const MLBROWSERUTILS_ORIGIN = "git@gitlab.com:eyeo/machine-learning/mlbrowserutils.git";
const MLBROWSERUTILS_TAG = "main";

const DIR_ROOT = join(dirname(fileURLToPath(import.meta.url)), "..");
const DIR_LIB = join(DIR_ROOT, "lib");
const DIR_MLBROWSERUTILS = join(DIR_ROOT, ".temp_mlbrowserutils");
const DIR_MLBROWSERUTILS_DIST = join(DIR_MLBROWSERUTILS, "dist");

const COL_CLEAR = "\u001b[0m";
const COL_CYAN = "\u001b[36m";
const COL_RED = "\u001b[31m";

// Check for build arguments:
// `npm run build [-- [--mlbu-repo <REPO_URL>] [--mlbu-tag <TAG>] [--mlbu-dir <PATH>] [--mlbu-no-test] [--mlbu-no-build]]`
// Examples:
// `npm run build -- --mlbu-repo ../mlbrowserutils/ --mlbu-tag feature_branch` for a local feature branch
// `npm run build -- --mlbu-repo https://example.com/repo.git --mlbu-tag tag_name` for a remote tag
// `npm run build -- --mlbu-dir /path/to/mlbu` for copying a local/unstaged working directory
// `npm run build -- --mlbu-no-test` will omit `npm run test` on mlbrowserutils
// `npm run build -- --mlbu-no-build` will omit `npm run build` on mlbrowserutils
let mlbrowserutilsBuild = !process.argv || !process.argv.includes("--mlbu-no-build");
let mlbrowserutilsTest = !process.argv || !process.argv.includes("--mlbu-no-test");
let mlbrowserutilsOrigin = process.argv && process.argv.includes("--mlbu-repo") ?
  process.argv[process.argv.indexOf("--mlbu-repo") + 1] : MLBROWSERUTILS_ORIGIN;
let mlbrowserutilsTag = process.argv && process.argv.includes("--mlbu-tag") ?
  process.argv[process.argv.indexOf("--mlbu-tag") + 1] : MLBROWSERUTILS_TAG;
let mlbrowserutilsDir = process.argv && process.argv.includes("--mlbu-dir") ?
  join(DIR_ROOT, process.argv[process.argv.indexOf("--mlbu-dir") + 1]) : "";
if (process.argv && process.argv.length >= 2 && !process.argv.some(arg => arg.startsWith("--mlbu"))) {
  // Backwards-compatibility support for `npm run build -- <repo> <branch>`
  if (process.argv[2])
    mlbrowserutilsOrigin = process.argv[2];
  if (process.argv[3])
    mlbrowserutilsTag = process.argv[3];
}
// Turn origin into absolute path if provided string isn't a URL and isn't already absolute
if (mlbrowserutilsOrigin && !mlbrowserutilsOrigin.includes("://") && mlbrowserutilsOrigin.slice(0, 1) !== "/" &&
  mlbrowserutilsOrigin.slice(1, 2) !== ":\\" && !mlbrowserutilsOrigin.startsWith("git@"))
  mlbrowserutilsOrigin = join(DIR_ROOT, mlbrowserutilsOrigin);

printHeadline("Building @eyeo/mlaf");

// Clean up
printProgress("Cleaning up");
cleanUpTemp();
fs.mkdirSync(DIR_MLBROWSERUTILS);

// Clone & build mlbrowserutils
if (mlbrowserutilsDir) {
  // If a directory is given, copy this instead of cloning mlbrowserutils
  // Used by mlbrowsuerutils `npm build-abp`
  printProgress(`Copying mlbrowserutils from ${mlbrowserutilsDir}`);
  exec("rsync", ["-avq", mlbrowserutilsDir, DIR_MLBROWSERUTILS, "--exclude", "temp-build-abp"], DIR_MLBROWSERUTILS);
}
else {
  // If no directory is given, clone the provided/default repo
  printProgress(`Cloning mlbrowserutils from ${mlbrowserutilsTag}@${mlbrowserutilsOrigin}`);
  exec("git", ["clone", mlbrowserutilsOrigin, "--branch", mlbrowserutilsTag, "--single-branch", "-c", "advice.detachedHead=false", DIR_MLBROWSERUTILS], DIR_MLBROWSERUTILS);
}
printProgress("Installing mlbrowserutils' dependencies");
exec("npm", ["i"], DIR_MLBROWSERUTILS);
if (mlbrowserutilsBuild) {
  printProgress("Building mlbrowserutils");
  exec("npm", ["run", "build"], DIR_MLBROWSERUTILS);
}
if (mlbrowserutilsTest) {
  printProgress("Testing mlbrowserutils");
  exec("npm", ["run", "test"], DIR_MLBROWSERUTILS);
}

// Populate lib
printProgress("Copying mlbrowserutils dist");
fs.rmSync(DIR_LIB, {recursive: true, force: true});
fs.cpSync(DIR_MLBROWSERUTILS_DIST, DIR_LIB, {recursive: true});

// Cleanup
printProgress("Cleaning up");
cleanUpTemp();

function printHeadline(headline) {
  if (headline) {
    process.stdout.write(`${COL_CYAN}${"-".repeat(14 + headline.length)}\n`);
    process.stdout.write(`${COL_CYAN}=== ${COL_CLEAR}MLAF: ${headline} ${COL_CYAN}===\n`);
    process.stdout.write(`${COL_CYAN}${"-".repeat(14 + headline.length)}\n`);
  }
}

function printProgress(text) {
  if (text)
    process.stdout.write(`${COL_CYAN}* MLAF: ${COL_CLEAR}${text}\n`);
}

function exec(command, args, cwd) {
  if (command) {
    try {
      let ret = spawnSync(command, args, {shell: true, stdio: "inherit", cwd});
      if (ret.status !== 0)
        die("Error during subprocess");
    }
    catch (err) {
      die(err);
    }
  }
}

function cleanUpTemp() {
  fs.rmSync(DIR_MLBROWSERUTILS, {recursive: true, force: true});
}

function die(err) {
  if (err)
    process.stdout.write(`${COL_RED}ERROR:${COL_CLEAR} ${err}\n`);
  cleanUpTemp();
  process.exit(1);
}
