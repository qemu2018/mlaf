/*
 * This file is part of eyeo's mlaf module (@eyeo/mlaf),
 * Copyright (C) 2006-present eyeo GmbH
 *
 * @eyeo/mlaf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * @eyeo/mlaf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with @eyeo/mlaf.  If not, see <http://www.gnu.org/licenses/>.
 */
/* globals atob getComputedStyle */

const DEFAULT_GRAPH_CUTOFF = 500;

/**
 * Turn a dom element into a graph
 * @param {Object} bundle - Model bundle as a JSON object
 * @param {Object} target - Dom element to turn into graph
 * @returns {Promise.<Object>} Returns a JSON object representing target as a graph
 */
async function domToGraph(bundle, target) {
  return new Promise((resolve, reject) => {
    if (!bundle || !target)
      return reject();

    let config = bundle.config;
    let cutoff = config.cutoff || bundle.topology.graphml.nodes || DEFAULT_GRAPH_CUTOFF;

    // Pre-compute node names to speed up traversal
    config = config.filter(e => e.include);
    for (let node of config)
      node.groupName = Object.keys(node)[2];

    let _extract = (element, data, group, attribute) => {
      if (group === "attributes" && typeof element.attributes[attribute] !== "undefined")
        data.attributes[attribute] = element.attributes[attribute].value;
      else if (group === "style" && element.style[attribute])
        data.attributes.style[attribute] = element.style[attribute];
      else if (group === "css")
        data.cssSelectors = getComputedStyle(element).cssText || "";
    };

    let _traverse = element => {
      cutoff -= 1;
      if (cutoff < 0)
        return;

      let data = {
        tag: element.tagName,
        width: element.clientWidth,
        height: element.clientHeight,
        attributes: {
          style: {}
        },
        children: []
      };

      for (let node of config) {
        for (let feature of node[node.groupName].features) {
          for (let [featureId, featureContent] of Object.entries(feature)) {
            if ("names" in featureContent) {
              for (let featureName of featureContent.names) {
                for (let featureNameFragment of featureName.split("^"))
                  _extract(element, data, node.groupName, featureNameFragment);
              }
            }
            else {
              _extract(element, data, node.groupName, featureId);
            }
          }
        }
      }

      if (element.children) {
        for (let child of element.children) {
          let childGraph = _traverse(child);
          if (childGraph)
            data.children.push(childGraph);
        }
      }

      return data;
    };

    let graph = _traverse(target);
    resolve(graph);
  });
}

const MESSAGE_PREFIX = "ML:";
const MESSAGE_PREPARE_SUFFIX = "prepare";
const MESSAGE_INFERENCE_SUFFIX = "inference";
const errors = {
  UNKNOWN_REQUEST: 1,
  MISSING_REQUEST_DATA: 2,
  UNKNOWN_MODEL: 3,
  MISSING_INFERENCE_DATA: 4,
  INFERENCE_FAILED: 5,
  MODEL_INSTANTIATION_FAILED: 6,
  MISSING_ENVIRONMENTAL_SUPPORT: 7
};

const IN_FRAME = window.self !== window.top;
const DEBUG_PREFIX = "mldebug:";
// Timeout in MS before request to service worker is cancelled
const SERVICE_WORKER_TIMEOUT = 10000;

// Fall back to (_ => _) instead of $ if $ is undefined. This
// happens when running tests outside of the ABP-Snippets'
// artifact.
let {
  console: console$,
  JSON: JSON$,
  Map: Map$,
  WeakSet: WeakSet$,
  MutationObserver: MutationObserver$
} = (typeof $ !== "undefined" ? $ : (_ => _))(window);

let modelConfigs = new Map$();

/**
 * Hides any HTML element that is classified as an ad by a machine learning
 * graph model.
 * @param {string} modelName The name of the model to be used for inference.
 * @param {string} selectors The CSS selector of an HTML element that is to
 *   be evaluated.
 *   Can be prefixed with `mldebug:` to trigger debugging output. E.g.:
 *     `*$#$hide-if-classifies mldebug:div.someclass`
 *   Can contain `..` to select the selected element's parent. E.g.:
 *     `*$#$hide-if-classifies div.someclass..span`
 *       Which will search for `span` within the parent of `div.someclass`.
 *     `*$#$hide-if-classifies div.someclass....:scope>div`
 *       Which will search for divs as direct ancestors of two parents up
 *       from `div.someclass`.
 * @param {?string} [inferenceTargetSelector] The CSS selector of an ancestor
 *   of the HTML element which will be used for inference. Defaults to the
 *   parent element if not specified. E.g.:
 *     `*$#$hide-if-classifies div.someclass div>span`
 *       Which will run inference on the second selector and hide the
 *       element selected via the first selector.
 *     `*$#$hide-if-classifies div.someclass..:scope>.headline div>span`
 *       Which will run inference on a `span` as a direct ancestor of a
 *       `div` found as a child of an element with the class `.headline`
 *       as a direct ancestor of the parent of `div.someclass`. In case of
 *       a match, `div.someclass` will be hidden.
 */
function hideIfClassifies(modelName, selectors, inferenceTargetSelector = "") {
  if (!modelName || !selectors)
    return;

  let debugMode = false;
  let elementBacklog = new Map$();
  let digestedElements = new WeakSet$();
  let elementObserver;

  // Turn on debug mode if filter starts with `mldebug:`
  if (selectors.startsWith(DEBUG_PREFIX)) {
    debugMode = true;
    selectors = $(selectors).slice(DEBUG_PREFIX.length);
  }

  // Split selector in preparation for pseudo-has traversal.
  // See fillBacklog() for details.
  selectors = $(selectors).split("..");

  let raceWinnerCallback = raceWinner(
    "hide-if-graph-matches",
    () => stop()
  );

  let fillBacklog = () => {
    // Parent traversal in case `..` is present in the selector
    // `div.foo..:scope>.bar`
    // translates to
    // `*:has(:scope>div.foo)>.bar`
    let targetContainers = [document];
    $(selectors).forEach((selector, index) => {
      // Traverse to target's parent for every selector except for the first
      if (index) {
        targetContainers = $(targetContainers)
          .reduce(($arr, e) => (e && $(e).parentElement ? $arr.concat($(e).parentElement) : $arr), $([]));
      }
      // Execute selector and store nodes if valid
      targetContainers = $(targetContainers).reduce(($arr, e) => {
        if (!selector) {
          // If selector is blank, leave targetContainer in the array
          $arr.push(e);
        }
        else {
          // If selector isn't blank replace targetContainer with the selected nodes
          // Ignore results from illegal selectors
          try {
            $arr = $arr.concat(...$(e).querySelectorAll(selector));
          }
          catch (err) { /* no-op */ }
        }
        return $arr;
      }, $([]));
    });

    for (let targetContainer of targetContainers) {
      let inferenceTargets = [targetContainer];
      if (inferenceTargetSelector) {
        try {
          inferenceTargets = $(targetContainer).querySelectorAll(inferenceTargetSelector);
        }
        catch (err) { /* no-op */ }
      }
      // Store all targets in the inference backlog which is digested once the corresponding model
      // is loaded and on every subsequent page mutation.
      if (!digestedElements.has(targetContainer))
        elementBacklog.set(targetContainer, inferenceTargets);
    }
  };

  let digestBacklog = () => {
    // Allow attempts to digest the backlog even if it's empty as long as the snippet
    // is invoked on the topmost frame. This improves the service worker boot up time.
    if (!elementBacklog.size && IN_FRAME)
      return;

    getModelConfig().then(modelConfig => {
      if (!modelConfig)
        return Promise.reject(`Config file for ${modelName} corrupted`);
      for (let [targetContainer, inferenceTargets] of elementBacklog.entries()) {
        for (let inferenceTarget of inferenceTargets) {
          debugOut(`Preparing inference for element ${modelName}, ${(targetContainer.outerHTML || "").slice(0, 100)}...`);
          domToGraph({config: modelConfig}, inferenceTarget).then(graph => {
            inference(graph, targetContainer);
          }).catch(e => debugOut(`domToGraph failed: ${e} ${(targetContainer.outerHTML || "").slice(0, 100)}...`, true));
        }

        digestedElements.add(targetContainer);
        elementBacklog.delete(targetContainer);
      }
    }).catch(err => {
      debugOut(err, true);
      stop();
    });
  };

  let getModelConfig = () => {
    // Return a promise which waits for the call to the service worker
    // to finish. Resolves to the model config. Promise is stored in a
    // map so other filter hits using the same model won't request the
    // configuration multiple times.
    if (modelConfigs.has(modelName))
      return modelConfigs.get(modelName);

    debugOut(`Preparing worker with model ${modelName}`);
    // Store a promise resolving to the config so subsequent
    // requests to the same model can reuse the initial request.
    let configRequest = new Promise((resolve, reject) => {
      let responseTimeout = setTimeout(() => {
        reject(`Worker preparation with ${modelName} failed: service worker timeout`);
      }, SERVICE_WORKER_TIMEOUT);
      chrome.runtime.sendMessage({
        type: MESSAGE_PREFIX + MESSAGE_PREPARE_SUFFIX,
        debug: debugMode,
        model: modelName
      }, response => {
        clearTimeout(responseTimeout);
        if (response && "config" in response) {
          debugOut(`Received config for ${modelName}: ${JSON$.stringify(response.config || {}).slice(0, 100)}...`);
          response.config.cutoff = response.cutoff;
          resolve(response.config);
        }
        else {
          reject(`Worker preparation with ${modelName} failed: ${JSON$.stringify(response || {})}`);
        }
      });
    });
    // Silently catch error in case the request is never digested
    configRequest.catch(err => { /* no-op */ });
    modelConfigs.set(modelName, configRequest);
    return configRequest;
  };

  let inference = (graph, targetContainer) => {
    debugOut(`Requesting inference for graph ${modelName}: ${JSON$.stringify(graph || {}).slice(0, 100)}...`);
    chrome.runtime.sendMessage({
      type: MESSAGE_PREFIX + MESSAGE_INFERENCE_SUFFIX,
      debug: debugMode,
      model: modelName,
      graph
    }, response => {
      if (response && "prediction" in response) {
        debugOut(`Received ${response.prediction} inference results with ${modelName} for graph ${$(JSON$.stringify(graph || {})).slice(0, 100)}...`);
        if (!debugMode && response.prediction) {
          raceWinnerCallback();
          hideElement(targetContainer);
        }
        else if (debugMode && response.prediction) {
          $(targetContainer).style.border = "5px solid #ff0000";
        }
        else if (debugMode && !response.prediction) {
          $(targetContainer).style.border = "5px solid #00ff00";
        }
      }
      else {
        debugOut(`Inference with ${modelName} for graph ${$(JSON$.stringify(graph || {})).slice(0, 100)} failed: ${JSON$.stringify(response || {})}`, true);
        // Stop sending more inference requests if the service worker fails to
        // run inference. This can happen due to missing OffscreenCanvas support
        // or lack of hardware acceleration.
        if ("error" in response && (response.error === errors.INFERENCE_FAILED ||
          response.error === errors.MISSING_ENVIRONMENTAL_SUPPORT))
          stop();
      }
    });
  };

  let debugOut = (message, error = false) => {
    if (debugMode) {
      console$.log(
        "%cMLDBG \u2665 %c| %s%c%s%c |",
        "color:cyan",
        error ? "color:red" : "color:inherit",
        message,
        "color:grey",
        IN_FRAME ? " (from iframe)" : "",
        "color:inherit"
      );
    }
  };

  let run = () => {
    fillBacklog();
    digestBacklog();
  };

  let stop = () => {
    elementObserver.disconnect();
    elementBacklog.clear();
  };

  if (!IN_FRAME)
    debugOut(`Filter hit for ${modelName}: ${selectors.join("..")} ${inferenceTargetSelector}`);

  elementObserver = new MutationObserver$(() => run());
  elementObserver.observe(document, {childList: true, subtree: true});
  run();
}

export { hideIfClassifies };
